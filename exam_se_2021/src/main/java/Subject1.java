interface C {
}

class E implements C {
    public int metG(int i) {
        return i;
    }

}

class F {
    public int metA() {
        return 0;
    }
}

class B {
    public long t;
    public int x() {
        return 0;
    }

}

class A {
    public int met(B b)  {
        return b;
    }

    class D {
    }

}